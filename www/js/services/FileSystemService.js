angular.module('LiveEngine.FileSystem.Services', [])

.service('FileSystemService', function($rootScope, $http, $ionicModal, $ionicPopup, LoggerService,  $q, HelperService) {

      var me = this;

      me.__chunkSize = 5 * 1024 * 1024;
      me.fileSystem = null;


      // ######################################## //
      // inizializzo il filesystem una volta sola //
      // ######################################## //
      me.InitializeFilesystem = function(parameters) {

          try {

            if(!window.cordova) {
                throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
            }

            window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function(fs) {

                me.fileSystem = fs.root;
                parameters.onComplete(null);

              }, function(err) {

                me.fileSystem = null;
                parameters.onComplete(err);

            });

          } catch(err) {

            me.fileSystem = null;
            parameters.onComplete(err);

          }

      };


      // parameters.directory
      // parameters.fileName
      // parameters.onComplete = function(err, isAvailable)
      me.fileExists = function(parameters) {

          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }


              me.fileSystem.getDirectory(parameters.directory, { create: true }, function (dinamicClassesDirEntry) {


                  dinamicClassesDirEntry.getFile(

                      parameters.fileName,

                      { create: false },

                      function(fileEntry) {
                          parameters.onComplete(null, true);
                      },

                      function() {
                          parameters.onComplete(null, false);
                      }

                  );

              }, function(err) {

                  parameters.onComplete(null, false);

              });

          } catch(err) {
              parameters.onComplete(err, false);
          }

      };


      // parameters.directory
      // parameters.fileName
      // parameters.bufferData
      // parameters.onComplete = function(err, operationCompleted)
      me.writeFile = function(parameters) {

          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }

              me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

                  directoryEntry.getFile(

                      parameters.fileName,

                      { create: true, exclusive: false },

                      function(fileEntry) {

                          fileEntry.createWriter(function(fileWriter) {

                              fileWriter.onwriteend = function(e) {
                                  parameters.onComplete(null, true);
                              };

                              fileWriter.onerror = function(e) {
                                  parameters.onComplete("file write error: " + e.toString(), false);
                              };

                              try {

                                  var blob = new Blob([parameters.bufferData], {type: 'text/plain'});
                                  fileWriter.write(blob);

                              } catch(blob_err) {

                                  try {

                                      var webkitBlob = new WebKitBlobBuilder();                                 
                                      webkitBlob.append(parameters.bufferData);
                                      fileWriter.write(webkitBlob.getBlob('text/plain'));

                                  } catch(webkit_blob_err) {

                                      parameters.onComplete("blob creation error", false);

                                  }
                                  
                              }

                          }, function(err) {
                              parameters.onComplete("file write error: " + JSON.stringify(err), false);

                          });

                      },

                      function() {
                          parameters.onComplete(null, false);
                      }

                  );

              }, function(err) {
                  parameters.onComplete(err, false);
              });

          } catch(err) {
              parameters.onComplete(err, false);
          }

      };


      // parameters.directory
      // parameters.fileName
      // parameters.onComplete = function(err, bufferData)
      me.fileRead = function(parameters) {

          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }

              me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

                  directoryEntry.getFile(

                      parameters.fileName,

                      { create: false, exclusive: false },

                      function(fileEntry) {


                          fileEntry.file(function(readedFile) {

                              var reader = new FileReader();

                              reader.onloadend = function(e) {
                                  parameters.onComplete(null, this.result);
                              };

                              reader.readAsText(readedFile);

                          }, function(e) {
                              parameters.onComplete("file read error: " + JSON.stringify(err), null);
                          });

                      }, function() {
                          parameters.onComplete('getFileError', null);
                      }

                  );

              }, function(err) {
                  parameters.onComplete(err, null);
              });

          } catch(err) {
              parameters.onComplete(err, null);
          }

      };


      // parameters.directory
      // parameters.fileName
      // parameters.onComplete = function(err)
      me.fileDelete = function(parameters) {

          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }

              me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

                  directoryEntry.getFile(

                      parameters.fileName,

                      { create: false, exclusive: false },

                      function(fileEntry) {

                          fileEntry.remove(function() {
                            parameters.onComplete(null);
                          }, function(err) {
                            parameters.onComplete(err);
                          });

                      }, function() {
                          parameters.onComplete('getFileError');
                      }

                  );

              }, function(err) {
                  parameters.onComplete(err);
              });

          } catch(err) {
              parameters.onComplete(err);
          }

      };



      me.getFileContentAsBase64 = function(parameters) {

          try {

              if(!window.cordova) {
                  throw LoggerService.KNOWN_ERROR.IS_IN_BROWSER;
              }

              me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

                  directoryEntry.getFile(

                      parameters.fileName,

                      { create: false, exclusive: false },

                      function(fileEntry) {

                          fileEntry.file(function(readedFile) {

                              var reader = new FileReader();

                              reader.onloadend = function(e) {
                                  parameters.onComplete(null, this.result);
                              };

                              reader.readAsDataURL(readedFile);

                          }, function(e) {
                              parameters.onComplete("file read error: " + JSON.stringify(err), null);
                          });

                      }, function() {
                          parameters.onComplete('getFileError', null);
                      }

                  );

              }, function(err) {
                  parameters.onComplete(err, null);
              });

          } catch(err) {
              parameters.onComplete(err, null);
          }



      };

      // parameters.url
      // parameters.directory
      // parameters.fileName
      // parameters.onComplete -> function(err, localFilePath)
      me.downloadFile = function(parameters) {

          me.fileSystem.getDirectory(parameters.directory, { create: true }, function (directoryEntry) {

              // Parameters passed to getFile create a new file or return the file if it already exists.
              directoryEntry.getFile(parameters.fileName, { create: true, exclusive: false }, function (fileEntry) {

                  var fileTransfer = new FileTransfer();
                  var fileURL = fileEntry.toURL();

                  fileTransfer.download(
                      parameters.url,
                      fileURL,

                      function (entry) {
                          parameters.onComplete(null, fileURL);
                      },

                      function (error) {
                          parameters.onComplete("[downloadFile] file download error: " + error.code, null);
                      }

                  );

              }, function(err) {

                  parameters.onComplete("[downloadFile] file create error: " + JSON.stringify(err), null);

              });

          }, function(err) {

            parameters.onComplete("[downloadFile] directory creation error" + JSON.stringify(err), null);

          });


      };

      me.getLocalPath = function()
      {
        return me.fileSystem.toURL();
      }


});
