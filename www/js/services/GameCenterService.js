angular.module('LiveEngine.GameCenter.Services', [])

.service('GameCenter', function(GlobalVariables, $rootScope, moment, FileSystemService, $ionicLoading,
SocketService, $ionicPopup, $timeout, $ionicModal, $ionicSlideBoxDelegate, $http, SupportServices, LanguageService) {

    var self = this;

    self.callbacks = [];

    self.device_events_callbacks = [];
    self.waitBox = null;
    self.managementWnd = null;
    self.SAVEDIR = 'GameCenter' + GlobalVariables.directory_trail;
    self.SAVEFILE = 'gc.json';

    self.configuration = {
      nickname: ''
    };

    self.gc_scope = $rootScope.$new(true);

    self.gc_scope.data = {
      
      managementWnd: null,
      curr_tab: 0,
      sliderDelegate: null,
      games: [],
      pending_requests: [],
      results: [],
      showDelete: false,

      hasPendingRequests: false,

      options: {
        loop: false,
        effect: 'none',
        speed: 500
      }

    }


    self.clearOlderRequests = function() {

        var NOW = (new Date()).getTime();

        for(var j = self.gc_scope.data.pending_requests.length - 1; j >= 0; j--) {
            
          var age = NOW - self.gc_scope.data.pending_requests[j].creation_time;

          if(age >= self.gc_scope.data.pending_requests[j].time_to_live) {                      
            self.removeRequest(self.gc_scope.data.pending_requests[j]);
          }

        }

        $timeout();

        setTimeout(function() {
          self.clearOlderRequests();
        }, 5000);

    }


    self.closeView = function(onClose) {
      self.gc_scope.closeView(function() {
        if(onClose) {
          onClose();
        }
      });
    }

    self.gc_scope.closeView = function(onClose) {
      self.gc_scope.data.managementWnd.remove().then(function() {
        if(onClose) {
          onClose();
        }
      });
    }

    
    self.gc_scope.setNickname = function() {
      
       
      self.openNicknameChooser({
        sub_title: null,
        onComplete: function(nickname) {}
      });

      
    }


    self.gc_scope.tap = function(item) {
      self.gc_scope.data.sliderDelegate.slideTo(item,0);
    }

    self.gc_scope.click_on_game = function(game) {

      game.classInstance[ game.onClickFunctionName ]();

    }

    self.gc_scope.click_on_invite = function(invitation) {
      
      invitation.classInstance[ invitation.onClickFunctionName ](invitation);

    }

    self.gc_scope.showDelete = function(invitation) {      
        self.gc_scope.data.showDelete = !self.gc_scope.data.showDelete;
    }

    self.gc_scope.removeRequest = function(invitation) {
        self.removeRequest(invitation);
    }

    self.gc_scope.deleteAllRequests = function() {


      var askDelete = $ionicPopup.confirm({
          title: LanguageService.getLabel('ELIMINARE_TUTTI_INVITI'),          
          cancelText: LanguageService.getLabel('NO'), 
          cancelType: 'button-stable',
          okText: LanguageService.getLabel('SI'), 
          okType: 'button-assertive'
      });

      askDelete.then(function(res) {

          if(res) {
              
              self.gc_scope.data.pending_requests = [];
              self.gc_scope.data.hasPendingRequests = false;
              $rootScope.game_center.pending_requests['testata'] = 0;
              $rootScope.game_center.pending_requests['progetto'] = 0;
              $rootScope.game_center.pending_requests['articolo'] = 0;                               

          }  

      });
      
    }

    self.Initialize = function() {

      $rootScope.game_center = {
          
          games_count: 0,

          pending_requests: {
              testata: 0,
              progetto: 0,
              articolo: 0
          }

      };

      self.loadConfig({
        onComplete: function(err) {
          console.log("Game center - load complete");
          console.log("Game center - Error: " + err);
        }
      });


      self.clearOlderRequests();


      
      document.addEventListener("pause", function() {        
        for(var key in self.device_events_callbacks) {
          for(var cbName in self.device_events_callbacks[key]['pause']) {             
              self.device_events_callbacks[key]['pause'][cbName]();
          }
        }
      }, false);



      document.addEventListener("resume", function() {        

        for(var key in self.device_events_callbacks) {
          for(var cbName in self.device_events_callbacks[key]['resume']) {             
              self.device_events_callbacks[key]['resume'][cbName]();
          }
        }
        
      }, false);



      // ########################################################## //
      // ricezione di un messaggio dal server di tipo 'game_invite' //
      // ########################################################## //     
      SocketService.setCallback('fnOnReceiveGameInvite', 'game_invite', function(message) {

          if(!(message.key && message.uid)) {
              return;
          }

          if(message.uid == GlobalVariables.deviceUUID) {
            return;
          }

          for(var cbName in self.callbacks[message.key]['game_invite']) {
              self.callbacks[message.key]['game_invite'][cbName](message);
          }

      });

      // ############################################################################# //
      // ricezione di un messaggio dal server di tipo 'game_invite_ack'. Può contenere
      // uno stato di risposta OK/KO
      // se la richiesta è OK posso sempre 'annullare' la mia richiesta di matchmaking
      // ############################################################################# //
      SocketService.setCallback('fnOnReceiveGameInviteAck', 'game_invite_ack', function(message) {
          
          if(!(message.key && message.uid)) {
              return;
          }

        
          for(var cbName in self.callbacks[message.key]['game_invite_ack']) {
              self.callbacks[message.key]['game_invite_ack'][cbName](message);
          }

      });

      SocketService.setCallback('fnOnReceiveGameInviteTimeout', 'game_invite_timeout', function(message) {

          if(!(message.key && message.uid)) {
              return;
          }
          
          for(var cbName in self.callbacks[message.key]['game_invite_timeout']) {
              self.callbacks[message.key]['game_invite_timeout'][cbName](message);
          }

      });

      SocketService.setCallback('fnOnReceiveGameInviteExpiredOrCancelled', 'game_invite_expired_or_cancelled', function(message) {

          if(!(message.key && message.uid)) {
              return;
          }
          
          for(var cbName in self.callbacks[message.key]['game_invite_expired_or_cancelled']) {
              self.callbacks[message.key]['game_invite_expired_or_cancelled'][cbName](message);
          }

      });

      SocketService.setCallback('fnOnReceiveGameInviteInvitedAccept', 'game_invite_invited_accept', function(message) {

          if(!(message.key && message.uid)) {
              return;
          }
          
          for(var cbName in self.callbacks[message.key]['game_invite_invited_accept']) {
              self.callbacks[message.key]['game_invite_invited_accept'][cbName](message);
          }

      });

      SocketService.setCallback('fnOnReceiveGameInviteInviterAccept', 'game_invite_inviter_accept', function(message) {

          if(!(message.key && message.uid)) {
              return;
          }
          
          for(var cbName in self.callbacks[message.key]['game_invite_inviter_accept']) {
              self.callbacks[message.key]['game_invite_inviter_accept'][cbName](message);
          }

      });

      SocketService.setCallback('fnOnReceiveGameUserJoined', 'game_invite_user_joined', function(message) {

          if(!(message.key && message.uid)) {
              return;
          }
          
          for(var cbName in self.callbacks[message.key]['game_invite_user_joined']) {
              self.callbacks[message.key]['game_invite_user_joined'][cbName](message);
          }

      });

      SocketService.setCallback('fnOnReceiveGameMessage', 'game_message', function(message) {

          if(!(message.key && message.uid)) {
              return;
          }
          
          for(var cbName in self.callbacks[message.key]['game_message']) {
              self.callbacks[message.key]['game_message'][cbName](message);
          }

      });

      SocketService.setCallback('fnOnReceiveGameWinByForfait', 'game_win_by_forfait', function(message) {

          if(!(message.key)) {
              return;
          }
          
          for(var cbName in self.callbacks[message.key]['game_win_by_forfait']) {
              self.callbacks[message.key]['game_win_by_forfait'][cbName](message);
          }

      });

      SocketService.setCallback('fnOnReceiveGameVictory', 'game_victory', function(message) {

          if(!(message.key)) {
              return;
          }
          
          for(var cbName in self.callbacks[message.key]['game_victory']) {
              self.callbacks[message.key]['game_victory'][cbName](message);
          }

      });

      SocketService.setCallback('fnOnReceiveGameLost', 'game_lost', function(message) {

          if(!(message.key)) {
              return;
          }
          
          for(var cbName in self.callbacks[message.key]['game_lost']) {
              self.callbacks[message.key]['game_lost'][cbName](message);
          }

      });


      $rootScope.game_center.openGameRequests = function() {
          self.openGameRequests();
      }

    };


    self.openGameRequests = function() {

      self.gc_scope.data.curr_tab = 0;

      self.gc_scope.$on("$ionicSlides.slideChangeEnd", function(event, data) {

        $timeout(function() {
          self.gc_scope.data.curr_tab = data.slider.activeIndex.toString();
        });        

      });
 

      self.gc_scope.$on("$ionicSlides.sliderInitialized", function (event, data) {

          // data.slider is the instance of Swiper
          self.gc_scope.data.sliderDelegate = data.slider;

          // se esiste almeno una richiesta in attesa vai direttamente al tab delle sfide 
          if(self.gc_scope.data.hasPendingRequests == true) {
            self.gc_scope.data.sliderDelegate.slideTo("1", 0);  
          }
          
      });
 


      self.gc_scope.data.managementWnd = $ionicModal.fromTemplate(

        '<ion-modal-view padding="true" cache-view="false" style="background-color:rgba(0,0,0,0.5); width:100%; height:100%; left:0px; right:0px; top:0px;">' +
              
              '<img src="img/top_arrow.png" style="position:absolute; top:45px; right:65px; z-index:9999;"></img>' +
              
              '<ion-content scroll="false" padding="false" class="game-center-popup">' +

                  '<a ng-click="setNickname()" style="position:absolute; top: 0px; left: 0px; z-index:99999;" class="button button-icon icon ion-ios-gear-outline"></a>' +

                  '<a ng-click="showDelete()" ng-if="data.curr_tab==\'1\' && data.hasPendingRequests" style="position:absolute; top: 0px; left: 45px; z-index:99999;" class="button button-icon icon ion-ios-toggle-outline"></a>' +

                  '<a ng-click="deleteAllRequests()" ng-if="data.curr_tab==\'1\' && data.hasPendingRequests" style="position:absolute; top: 0px; left: 95px; z-index:99999;" class="button button-icon icon ion-ios-trash-outline"></a>' +

                  '<a ng-click="closeView()" style="position:absolute; top: 0px; right: 0px; z-index:99999;" class="button button-icon icon ion-ios-close"></a>' +

                  '<div style="position:absolute; top:45px; left:0px; right:0px; bottom:34px;">' + 
                      
                        '<ion-slides options="data.options" slider="data.sliderDelegate">' +

                          // ################# //
                          // pagina dei giochi //
                          // ################# //
                          '<ion-slide-page style="background-color:transparent;">' +
                              
                              '<ion-content class="game-ion-content" overflow-scroll="true" scroll="true">' +

                                '<h3>' + LanguageService.getLabel('TITOLO_TAB1_GC') + '</h3>' +
                                  
                                '<ion-list>' +

                                    '<ion-item ng-repeat="game in data.games" style="background-image:url({{game.background}}); border:solid 1px {{game.color}}; box-shadow: 2px 2px 2px {{game.color}};" class="game-item" ng-click="click_on_game(game)">' +

                                        '<div class="game-item-inner">' +
                                            '<div class="game-title"><i class="{{game.icon}} game-icon-cst" style="color:{{game.color}}; opacity: 0.9;"></i> {{game.title}}</div>' +
                                            '<div style="margin-right:8px;">{{game.description}}</div>' +
                                            '<div ng-if="game.second_icon" style="position:absolute; right:5px; bottom:2px; color:grey;"><i class="{{game.second_icon}}"></i></div>' +
                                        '</div>' +

                                    '</ion-item>' +

                                '</ion-list>' +

                              '</ion-content>' +

                          '</ion-slide-page>' +
                          // ################# //
                          // pagina dei giochi //
                          // ################# //

                          // ###################### //
                          // pagina delle richieste //
                          // ###################### //
                          '<ion-slide-page style="background-color:transparent;">' +
                            '<ion-content class="game-ion-content" overflow-scroll="true" scroll="true">' +

                                '<h3>' + LanguageService.getLabel('TITOLO_TAB2_GC') + '</h3>' +
                                  
                                '<ion-list ng-if="data.pending_requests.length > 0" show-delete="data.showDelete">' +

                                    '<ion-item ng-repeat="req in data.pending_requests" style="background-image:url({{req.background}});" class="request-item" ng-click="click_on_invite(req)">' +

                                        '<div class="request-item-inner">' +
                                            '<div class="request-title">{{req.title}}</div>' +
                                            '<div class="request-row"><i class="ion-person"></i>: {{req.from.user}}</div>' +
                                            '<div class="request-row"><i class="ion-clock"></i>: {{req.time}}</div>' +
                                        '</div>' +

                                        '<ion-delete-button class="ion-minus-circled" ng-click="removeRequest(req)"></ion-delete-button>' +

                                    '</ion-item>' +

                                '</ion-list>' +

                                '<div ng-if="data.pending_requests.length == 0">' + 
                                  '<div style="text-align:center; font-size:1.3em; padding:1em;">' +
                                    '<i style="color:#a78c23;" class="ion-sad"></i>&nbsp; ' + LanguageService.getLabel('NO_SFIDE_DA_ACCETTARE') + 
                                  '</div>' +
                                '</div>' +

                              '</ion-content>' +
                          '</ion-slide-page>' +
                          // ###################### //
                          // pagina delle richieste //
                          // ###################### //

                          // ######################## //
                          // pagina delle statistiche //
                          // ######################## //
                          '<ion-slide-page style="background-color:transparent;">' +
                            '<ion-content class="game-ion-content" overflow-scroll="true" scroll="true">' +

                                '<h3>' + LanguageService.getLabel('TITOLO_TAB3_GC') + '</h3>' +
                                                                
                                '<ion-list ng-if="data.results.length > 0">' +

                                    '<ion-item ng-repeat="result in data.results" class="request-item">' +

                                        '<div class="request-item-stat">' + 
                                          '<i class="ion-trophy"></i> '+LanguageService.getLabel('HAI_GIOCATO_A')+' <strong>{{result.title}}</strong> <span style="font-style:italic;">{{result.giocate}}</span> '+LanguageService.getLabel('VOLTE_VINCENDO')+' <span style="font-style:italic;">{{result.vinte}}</span> '+LanguageService.getLabel('SFIDE') + 
                                        '</div>' +

                                    '</ion-item>' +

                                '</ion-list>' +

                                '<div ng-if="data.results.length == 0">' + 
                                  '<div style="text-align:center; font-size:1.3em; padding:1em;">' +
                                    '<i style="color:#a78c23;" class="ion-sad"></i>&nbsp; ' + LanguageService.getLabel('NO_PARTECIPATO_SFIDE') + 
                                  '</div>' +
                                '</div>' +
                            

                              '</ion-content>' +
                          '</ion-slide-page>' +
                          // ######################## //
                          // pagina delle statistiche //
                          // ######################## //

                        '</ion-slides>' +


                  '</div>' +
                  
                  '<div style="position:absolute; bottom:0px; left:0px; right:0px; height:33px; padding:0px; line-height:25px;" class="row">' + 

                      '<div ng-class="{\'tab-cst-selected\': data.curr_tab == \'0\'}" class="col tab-cst" ng-click="tap(\'0\')"><i class="ion-ios-analytics-outline"></i> '+LanguageService.getLabel('GIOCHI')+'</div>' +
                      '<div ng-class="{\'tab-cst-selected\': data.curr_tab == \'1\'}" class="col tab-cst" ng-click="tap(\'1\')"><i class="ion-person-add"></i> '+LanguageService.getLabel('INVITI')+'</div>' +
                      '<div ng-class="{\'tab-cst-selected\': data.curr_tab == \'2\'}" class="col tab-cst" ng-click="tap(\'2\')"><i class="ion-stats-bars"></i> '+LanguageService.getLabel('STATISTICHE')+'</div>' +
              
                  '</div>' +
                
              '</ion-content>' +

          '</ion-modal-view>', {
              scope: self.gc_scope,
              animation :'none',
              hardwareBackButtonClose: false
          });

      
          self.gc_scope.data.managementWnd.show();

    };

    self.loadConfig = function(parameters) {

        FileSystemService.fileExists({
            directory: self.SAVEDIR,
            fileName: self.SAVEFILE,
            onComplete: function(err, isAvailable) {

                if(err) {

                    parameters.onComplete(err);

                } else {

                    if(!isAvailable) {

                        parameters.onComplete(null);

                    } else {

                        FileSystemService.fileRead({
                            directory: self.SAVEDIR,
                            fileName: self.SAVEFILE,
                            onComplete: function(err, bufferData) {

                                try {

                                    if(err)
                                      throw err;

                                    self.configuration = bufferData ? JSON.parse(bufferData) : {};
                                    parameters.onComplete(null);

                                } catch(err) {

                                    parameters.onComplete(err);

                                }

                            }
                        });

                    }
                }
            }
        });

    };


    self.saveConfig = function(parameters) {

        FileSystemService.writeFile({
            directory: self.SAVEDIR,
            fileName: self.SAVEFILE,
            bufferData: JSON.stringify(self.configuration),
            onComplete: function(err, operationCompleted) {

                if(err) {
                    parameters.onComplete(err);
                } else {

                    if(operationCompleted == false) {
                        parameters.onComplete("save file definition not completed");
                    } else {
                        parameters.onComplete(null);
                    }
                }

            }

        });

    };


    self.registerGame = function(params) {
 
        if(!(params && params.key)) {
          throw 'Parametro "key" obbligatorio'
        }

        if(!(params && params.title)) {
          throw 'Parametro "title" obbligatorio'
        }

        if(!(params && params.description)) {
          throw 'Parametro "description" obbligatorio'
        }

        if(!(params && params.classInstance && (typeof params.classInstance == 'object'))) {
          throw 'Parametro "classInstance" obbligatorio'
        }

        if(!(params && params.onClickFunctionName)) {
          throw 'Parametro "onClickFunctionName" obbligatorio'
        }

        var existing_game = _.find(self.gc_scope.data.games, function(game) {
          return (game.key == params.key && game.title == params.title);
        });

        if(existing_game) {
          return;
        }



        self.gc_scope.data.games.push({
          key: params.key,
          icon: params.icon ? params.icon : '',
          second_icon: params.second_icon || '',
          color: params.color ? params.color : 'grey',
          index: params.index || 0, 
          title: params.title,
          description: params.description,
          classInstance: params.classInstance,
          background: params.background || './img/background_game.jpg',
          onClickFunctionName: params.onClickFunctionName
        });

        self.gc_scope.data.games = _.sortBy(self.gc_scope.data.games, function(g) { return g.index; });

        $rootScope.game_center.games_count++;

    };



    self.unregisterGame = function(params) {

      self.gc_scope.data.games = 
        _.without(self.gc_scope.data.games, 
          _.find(self.gc_scope.data.games, function(item) { return params.key == item.key; }));

      if($rootScope.game_center.games_count > 0) {
          $rootScope.game_center.games_count--;  
      }


      
      
    }

    self.removeGameStat = function(key) {
        self.gc_scope.data.results = 
          _.without(self.gc_scope.data.results, 
            _.find(self.gc_scope.data.results, function(res) { return res.key == key; }));
    };


    self.loadStats = function(parameters) {

        if(!parameters.key) {
          throw 'Parametro "key" obbligatorio';
        }

        var __SAVE_FILE = parameters.key.replace(/\./g, '_') + "_stat.json";
        
 
        FileSystemService.fileExists({
            directory: self.SAVEDIR,
            fileName: __SAVE_FILE,
            onComplete: function(err, isAvailable) {

                if(err) {

                    parameters.onComplete(err, null);

                } else {

                    if(!isAvailable) {

                        parameters.onComplete('not available', null);

                    } else {

                        FileSystemService.fileRead({
                            directory: self.SAVEDIR,
                            fileName: __SAVE_FILE,
                            onComplete: function(err, bufferData) {

                                if(err) {
                                    
                                    parameters.onComplete(err, null);

                                } else {

                                    if(!bufferData) {

                                        parameters.onComplete('no data', null);

                                    } else {

                                        try {
                                
                                            var stats = JSON.parse(bufferData);
                                            parameters.onComplete(null, stats);

                                            if(stats.giocate && stats.giocate > 0) {

                                              self.gc_scope.data.results.push({
                                                key: parameters.key,
                                                title: parameters.title,
                                                giocate: stats.giocate,
                                                vinte: stats.vinte,
                                              });

                                            }
                                            

                                        } catch(err) {

                                            parameters.onComplete(err, null);

                                        }

                                    }

                                }                            

                            }

                        });

                    }
                
                }             

            }

        });   

    };


    self.alterStat = function(parameters) {

        if(!parameters.key) {
          throw 'Parametro "key" obbligatorio';
        }

        var game_stats = _.find(self.gc_scope.data.results, function(res) { return res.key == parameters.key; });

        if(!game_stats) {

          game_stats = {
            key: parameters.key,
            title: parameters.title,
            giocate: 0,
            vinte: 0,
          };

          self.gc_scope.data.results.push(game_stats);
        }

        game_stats[parameters.stat_name]++;

        $timeout();

        self.saveStats({
          key: parameters.key,
          stats: {
            giocate: game_stats.giocate,
            vinte: game_stats.vinte
          },
          onComplete: function(err) {
            if(err) {
              console.log(err);
            }
          }
        });

    };


    self.saveStats = function(parameters) {

        if(!parameters.key) {
          throw 'Parametro "key" obbligatorio';
        }

        if(!parameters.stats) {
          throw 'Parametro "stats" obbligatorio';
        }

        var __SAVE_FILE = parameters.key.replace(/\./g, '_') + "_stat.json";
        


        FileSystemService.writeFile({
            
            directory: self.SAVEDIR,
            fileName: __SAVE_FILE,

            bufferData: JSON.stringify(parameters.stats),

            onComplete: function(err, operationCompleted) {

                if(err) {
                    parameters.onComplete(err);
                } else {

                    if(operationCompleted == false) {
                        parameters.onComplete("save file definition not completed");
                    } else {
                        parameters.onComplete(null);
                    }
                }

            }

        });

         

    };

 
    self.setMessageCallback = function(widget_key, message_type, callbackName, callback) {

        if(!self.callbacks[widget_key]) {
          self.callbacks[widget_key] = [];
        }

        if(!self.callbacks[widget_key][message_type]) {
          self.callbacks[widget_key][message_type] = [];
        }

        delete self.callbacks[widget_key][message_type][callbackName];

        if(callback) {
            self.callbacks[widget_key][message_type][callbackName] = callback;
        }

    };


    self.setDeviceEventCallback = function(widget_key, event_type, callbackName, callback) {

        if(!self.device_events_callbacks[widget_key]) {
          self.device_events_callbacks[widget_key] = [];
        }

        if(!self.device_events_callbacks[widget_key][event_type]) {
          self.device_events_callbacks[widget_key][event_type] = [];
        }

        delete self.device_events_callbacks[widget_key][event_type][callbackName];

        if(callback) {
            self.device_events_callbacks[widget_key][event_type][callbackName] = callback;
        }


    };


    self.appendRequest = function(params) {
      
      if(!(params && params.match_id)) {
        throw "Parametro 'match_id' obbligatorio";
      }

      if(!(params && params.level)) {
        throw "Parametro 'level' obbligatorio";
      }

      if(!(params && params.key)) {
        throw "Parametro 'key' obbligatorio";
      }

      if(!(params && params.from)) {
        throw "Parametro 'from' obbligatorio";
      }

      if(!(params && params.classInstance && (typeof params.classInstance == 'object'))) {
        throw 'Parametro "classInstance" obbligatorio'
      }

      if(!(params && params.onClickFunctionName)) {
        throw 'Parametro "onClickFunctionName" obbligatorio'
      }

      if(!(params && params.time_to_live)) {
        throw 'Parametro "time_to_live" obbligatorio'
      }

      $rootScope.game_center.pending_requests[params.level]++;

      self.gc_scope.data.pending_requests.unshift({
          match_id: params.match_id,
          level: params.level,
          key: params.key,          
          time: moment().format('LLL'),
          from: params.from,
          title: params.title,
          creation_time: (new Date()).getTime(),
          time_to_live: params.time_to_live,
          background: params.background || './img/background_game.jpg',
          classInstance: params.classInstance,
          onClickFunctionName: params.onClickFunctionName
      });

      self.gc_scope.data.hasPendingRequests = true;

      $timeout();

      try {

        GlobalVariables.beepSound.play();  
      
      } catch(err) {
        console.log(err);
        console.log("Errore playing sound...");
      
      }
      
    
    };


    self.removeRequest = function(request) {

        self.gc_scope.data.pending_requests = 
          _.without(self.gc_scope.data.pending_requests, 
            _.find(self.gc_scope.data.pending_requests, function(item) { return request.match_id == item.match_id; }));

        $rootScope.game_center.pending_requests[request.level]--;

        self.gc_scope.data.hasPendingRequests = 
          ($rootScope.game_center.pending_requests['testata'] + $rootScope.game_center.pending_requests['progetto'] + $rootScope.game_center.pending_requests['articolo']) > 0;

    }

    // deprecato
    self.getConfiguration = function(params) {
        
        if(!(params && params.key)) {
          throw "Parametro 'key' obbligatorio";
        }

        $http({
            url: GlobalVariables.application.applicationUrl + '/configurable/progetto/' + GlobalVariables.application.currentProgetto.idProgetto + '/' + params.key,
            responseType: 'json',
            method: 'GET',
            timeout: 10000
        }).then(function(json) {
            params.onComplete(null, json.data);
        }, function(error, xhr) {
            params.onComplete(LanguageService.getLabel('PROBLEMI_RETE'), null);
        });
        
    };


    // sostituisce 'self.getConfiguration'
    self.getGameConfig = function(params) {
        
        if(!(params && params.key)) {
          throw "Parametro 'key' obbligatorio";
        }

        if(!(params && params.identifier)) {
          throw "Parametro 'identifier' obbligatorio";
        }

        if(!(params && params.livello)) {
          throw "Parametro 'livello' obbligatorio";
        }

        $http({
            url: GlobalVariables.application.applicationUrl + '/configurable/' + params.livello + '/' + params.identifier + '/' + params.key,
            responseType: 'json',
            method: 'GET',
            timeout: 10000
        }).then(function(json) {
            params.onComplete(null, json.data);
        }, function(error, xhr) {
            params.onComplete(LanguageService.getLabel('PROBLEMI_RETE'), null);
        });
        
    };





    // usato dai giochi per cui occorre SOLO il nickname //
    self.getNickname = function(params) {

        if(self.configuration.nickname) {

            params.onComplete(self.configuration.nickname);
        
        } else {
          
            self.openNicknameChooser({
                sub_title: params.sub_title,
                onComplete: function(nickname) {
                    params.onComplete(nickname);
                }
            });    

        }

    };



    self.getNicknameAndUserData = function(params) {

        if(self.configuration.nickname && self.configuration.email) {

            params.onComplete(self.configuration.nickname, self.configuration.email);
        
        } else {

            self.openNicknameChooser({
                sub_title: params.sub_title,
                onComplete: function(nickname, email) {
                    params.onComplete(nickname, email);
                }
            }); 

        }

    }



    self.openNicknameChooser = function(params) {

        var settings_scope = $rootScope.$new(true);   

        settings_scope.data = {
          show_disclaimer: false,
          tmp_nickname: self.configuration.nickname || ('Utente_' + (new Date()).getTime()),
          tmp_email: self.configuration.email || "",
          disclaimer_is_checked: self.configuration.email ? true : false
        };


        settings_scope.showHideDisclaimer = function() {
            settings_scope.data.show_disclaimer = !settings_scope.data.show_disclaimer;
        };


        settings_scope.closeSettignsWnd = function() {
            settings_scope.settingsWnd.remove();
        };

        settings_scope.resetDisclaimerCheckbox = function() {
            settings_scope.data.disclaimer_is_checked = false;
        }

        settings_scope.acceptSettignsWnd = function() {

            //controllo nickname
            if(!settings_scope.data.tmp_nickname) {              
                
                $ionicPopup.alert({
                   title: LanguageService.getLabel('ATTENZIONE'),
                   template: LanguageService.getLabel('DEVI_INSERIRE_NICKNAME')
                });

                return;
            }  

            // il nickname è stato selezionato. posso procedere
            self.configuration.nickname = settings_scope.data.tmp_nickname;

            // NON è stata inserita l'email. posso uscire dalla finestra
            if(!settings_scope.data.tmp_email) {
                                              
                self.saveConfig({
                  onComplete: function(err) {
                    settings_scope.settingsWnd.remove().then(function() {
                        params.onComplete(self.configuration.nickname, null);
                        settings_scope.$destroy();
                        delete settings_scope;
                    });
                  }
                });                                        

            } else {

                //controllo formato email
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

                if(!re.test(settings_scope.data.tmp_email)) {

                    $ionicPopup.alert({
                       title: LanguageService.getLabel('ATTENZIONE'),
                       template: LanguageService.getLabel('EMAIL_FORMATO_ERRATO')
                    });

                    return;

                }                

                //controllo accettazione disclaimer
                if( settings_scope.data.disclaimer_is_checked == false ) {

                    $ionicPopup.alert({
                       title: LanguageService.getLabel('ATTENZIONE'),
                       template: LanguageService.getLabel('OBBLIGATORIO_ACCETTARE_DISCLAIMER')
                    });

                    return;

                }

                


                $ionicLoading.show({
                    template: LanguageService.getLabel('ATTENDERE')
                });

                $http({
                    url: GlobalVariables.application.applicationUrl + "/user_info",
                    responseType: 'json',
                    method: 'POST',
                    timeout: 10000,
                    data: {
                      uid: GlobalVariables.deviceUUID,
                      email: settings_scope.data.tmp_email
                    }
                })
                .then(function(json) {

                    $ionicLoading.hide();
                    self.configuration.email = settings_scope.data.tmp_email;

                    self.saveConfig({
                      onComplete: function(err) {
                        settings_scope.settingsWnd.remove().then(function() {
                            params.onComplete(self.configuration.nickname, self.configuration.email);
                            settings_scope.$destroy();
                            delete settings_scope;
                        });
                      }
                    });  

                    settings_scope.settingsWnd.remove().then(function() {
                        
                        
                    });

                }, function(err) {

                    $ionicLoading.hide();

                    $ionicPopup.alert({

                       title: LanguageService.getLabel('ATTENZIONE'),
                       template: LanguageService.getLabel('ERRORE_INTERNO')
                    });

                });

                

            }



        };


        settings_scope.settingsWnd = $ionicModal.fromTemplate(

          '<ion-modal-view padding="true" class="nickname-chooser-wnd" cache-view="false">' +
 

                  '<div ng-if="!data.show_disclaimer" class="nickname-chooser-wnd-inner">' +

                      '<div style="padding: 5px 2px; letter-spacing:-1px; font-size:1.1em;">&rightarrow; ' + LanguageService.getLabel('WND_NICKNAME_PT1') + '</div>' + 

                      '<label class="item item-input lve-input"><input type="text" placeholder="Nickname" ng-model="data.tmp_nickname"></label>' + 


                       
                        '<div style="letter-spacing:-1px; padding:20px 2px 5px 2px; font-size:1.1em; color: #4d4d4d;">&rightarrow; ' + LanguageService.getLabel('WND_NICKNAME_PT2') + '</div>' +
                        


                        (params.sub_title ? '<div style="letter-spacing:-1px; padding: 0px 2px 5px 2px; color: #8a1616; font-size:1.1em;">' + params.sub_title + '</div>' : '') +                      

                        '<label class="item item-input lve-input"><input type="text" placeholder="Email" ng-change="resetDisclaimerCheckbox()" ng-model="data.tmp_email"></label>' + 
                       
                        '<div class="checkboxFive">'+
                            '<input type="checkbox" ng-model="data.disclaimer_is_checked" ng-checked="data.disclaimer_is_checked" id="checkboxFiveInput" name="" />' +
                            '<label for="checkboxFiveInput"></label>' +
                            '<div style="position: absolute; top: 0px; left: 45px;">' + 
                              '<button ng-click="showHideDisclaimer()" class="button button-clear button-positive" style="min-height: 0px; padding:0px; margin: 0px; line-height: 28px;">Disclaimer</button>' + 
                            '</div>' +
                        '</div>' +
                       

                      '<div class="row" style="position:absolute; bottom:5px; padding:0px; left:5px; right:5px; width:auto;">' +

                        '<div class="col" style="padding: 0px 2.5px 0px 0px;">' +
                          '<button ng-click="acceptSettignsWnd()" class="button button-full button-balanced" style="margin:0px;">'+LanguageService.getLabel('ACCETTA')+'</button>' +
                        '</div>' +

                        '<div class="col" style="padding: 0px 0px 0px 2.5px;">' +
                            '<button ng-click="closeSettignsWnd()" class="button button-full button-stable" style="margin:0px;">'+LanguageService.getLabel('ANNULLA')+'</button>' +
                        '</div>' +

                      '</div>' +
                      



                  '</div>' +

                  '<div ng-if="data.show_disclaimer" class="nickname-chooser-wnd-inner">' +

                      '<div style="position:absolute; top:0px; left:0px; right:0px; bottom: 55px;">' +
                          '<iframe src="' + (GlobalVariables.baseUrl + "/game_disclaimers/" + GlobalVariables.application.currentTestata.idTestata + "/disclaimer_"+GlobalVariables.systemLanguage+".html") + '" style="position:absolute; top:0px; bottom:0px; left:0px; right:0px; width:100%; height:100%;"></iframe>' +
                      '</div>' +

                      '<div class="row" style="position:absolute; bottom:5px; padding:0px; left:5px; right:5px; width:auto;">' +

                        '<div class="col" style="padding: 0px 0px 0px 0px;">' +
                          '<button ng-click="showHideDisclaimer()" class="button button-full button-stable" style="margin:0px;">'+LanguageService.getLabel('CHIUDI')+'</button>' +
                        '</div>' +

                      '</div>' +

                  '</div>' +
 

          '</ion-modal-view>',

          {
              scope: settings_scope,
              focusFirstInput: true,
              animation :'none',
              hardwareBackButtonClose: false,
              backdropClickToClose: false
          }

      );

      settings_scope.settingsWnd.show();

    };



 




});
